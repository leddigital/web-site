-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: sila
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `info1` text,
  `info2` text,
  `info3` text,
  `link1` text,
  `link2` text,
  `link3` text,
  `path` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `banners` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (9,'Sofás','sofas','TEste','sfsdfsdfsdfsdfsdfsdfsdf','categoria_5dc087da038eb.jpeg'),(10,'Mesas','mesas','sdfsdfsdfsdfs','sdfsdfsdfsdfsdf','categoria_5dc087f9ddf4d.jpeg');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_has_contents`
--

DROP TABLE IF EXISTS `categories_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL,
  PRIMARY KEY (`categories_id`,`contents_id`),
  KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  KEY `fk_categories_has_contents_categories1_idx` (`categories_id`),
  CONSTRAINT `fk_categories_has_contents_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_categories_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_has_contents`
--

LOCK TABLES `categories_has_contents` WRITE;
/*!40000 ALTER TABLE `categories_has_contents` DISABLE KEYS */;
INSERT INTO `categories_has_contents` VALUES (9,45),(10,46),(10,47);
/*!40000 ALTER TABLE `categories_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `short_description` text,
  `content` longtext,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alta` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (45,'Lorem Ipsum',NULL,'asdasdasdsad',NULL,'produto','lorem-ipsum','produto_5dc17900d6f57.jpeg','alta_5dc17900d758b.jpeg','2019-11-05 13:28:32','2019-11-05 13:28:32'),(46,'Mesa Camila',NULL,'asdasdasdasdasdasdasdasd',NULL,'produto','mesa-camila','produto_5dc1792681a3c.jpeg','alta_5dc1792682902.jpeg','2019-11-05 13:29:10','2019-11-05 13:29:10'),(47,'Mesa Ônix',NULL,'sdfsdfsdfsdfsdfsdf',NULL,'produto','mesa-onix','produto_5dc17aed3edfb.jpeg','alta_5dc17aed3f5f3.jpeg','2019-11-05 13:36:45','2019-11-05 13:40:18');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_contents`
--

DROP TABLE IF EXISTS `contents_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL,
  PRIMARY KEY (`contents_id`,`contents_child_id`),
  KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  KEY `fk_contents_has_contents_contents1_idx` (`contents_id`),
  CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_contents`
--

LOCK TABLES `contents_has_contents` WRITE;
/*!40000 ALTER TABLE `contents_has_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_images`
--

DROP TABLE IF EXISTS `contents_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text,
  PRIMARY KEY (`id`,`contents_id`),
  KEY `fk_contents_images_contents_idx` (`contents_id`),
  CONSTRAINT `fk_contents_images_contents` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_images`
--

LOCK TABLES `contents_images` WRITE;
/*!40000 ALTER TABLE `contents_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` text,
  `number` varchar(45) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text,
  `facebook` text,
  `linkedin` text,
  `twitter` text,
  `pinterest` text,
  `phone1` varchar(45) DEFAULT NULL,
  `email` text,
  `phone2` varchar(45) DEFAULT NULL,
  `texto_1` text,
  `texto_2` text,
  `valor_1` text,
  `valor_2` text,
  `valor_3` text,
  `valor_4` text,
  `valor_5` text,
  `valor_texto_1` text,
  `valor_texto_2` text,
  `valor_texto_3` text,
  `valor_texto_4` text,
  `valor_texto_5` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'Av. Marginal Tancredo Neves','300','Parque Industrial Tancredo Neves','15076-630','São José do Rio Preto','SP','(17) 99728-3179','https://www.instagram.com/siladecorr/','https://www.facebook.com/siladecorr/',NULL,NULL,NULL,'(17) 99728-3179','contato@siladecor.com.br','(17) 99728-3179','Cras faucibus faucibus urna vel fermentum. Nullam eu tempor urna. Morbi vel magna id augue commodo venenatis ac in neque. Ut hendrerit quam quis ligula feugiat luctus. Vivamus pulvinar elit a pretium pharetra.','Cras faucibus faucibus urna vel fermentum. Nullam eu tempor urna. Morbi vel magna id augue commodo venenatis ac in neque. Ut hendrerit quam quis ligula feugiat luctus. Vivamus pulvinar elit a pretium pharetra.','Duis at augue','Nulla pharetra','Sed dictum','Quisque vel','Aliquam iaculis','Maecenas vitae urna ut est ultrices bibendum. Integer ac imperdiet elit, a dapibus dui. Sed nec ultrices arcu. Integer non ipsum turpis. Quisque vel est ut libero scelerisque mollis eu vel mauris.','Maecenas vitae urna ut est ultrices bibendum. Integer ac imperdiet elit, a dapibus dui. Sed nec ultrices arcu. Integer non ipsum turpis. Quisque vel est ut libero scelerisque mollis eu vel mauris.','Maecenas vitae urna ut est ultrices bibendum. Integer ac imperdiet elit, a dapibus dui. Sed nec ultrices arcu. Integer non ipsum turpis. Quisque vel est ut libero scelerisque mollis eu vel mauris.','Maecenas vitae urna ut est ultrices bibendum. Integer ac imperdiet elit, a dapibus dui. Sed nec ultrices arcu. Integer non ipsum turpis. Quisque vel est ut libero scelerisque mollis eu vel mauris.','Maecenas vitae urna ut est ultrices bibendum. Integer ac imperdiet elit, a dapibus dui. Sed nec ultrices arcu. Integer non ipsum turpis. Quisque vel est ut libero scelerisque mollis eu vel mauris.');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'leddigital','digital@agencialed.com.br',NULL,'$2y$10$6NsTVvIenPGcOpcmLpjxIeS5p5Q4oTK59ThKMj9kuCnRgIwisgUgq','fRxlJZDRSNlkoUzwym7xsJ9XUQvn5Qe9E5x951FoO5Wlqxy61qpoTYDWuXp8','2019-09-26 20:22:30','2019-09-26 20:22:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'sila'
--

--
-- Dumping routines for database 'sila'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-05 10:50:14
