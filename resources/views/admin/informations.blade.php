@extends('layouts.admin')
@section('title', 'Informações do Site')
@section('content')
<header class="page-header">
    <h2>Informações Básicas</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Informações Básicas</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action={{ route('information.save') }}
            data-reload="{{ route('information.index') }}">
            @csrf

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Redes Sociais e Contato</h2>
                    <p class="card-subtitle">
                        Informações de redes sociais e contato.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-facebook"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->facebook)?$entity->facebook:'' }}" type="text" name="facebook"
                                            class="form-control" placeholder="facebook">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-instagram"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->instagram)?$entity->instagram:'' }}" type="text" name="instagram"
                                            class="form-control" placeholder="instagram">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-linkedin"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ isset($entity->linkedin)?$entity->linkedin:'' }}" name="linkedin"
                                            class="form-control" placeholder="linkedin">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-twitter"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ isset($entity->twitter)?$entity->twitter:'' }}" name="twitter"
                                            class="form-control" placeholder="twitter">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-pinterest"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="pinterest" value="{{ isset($entity->pinterest)?$entity->pinterest:'' }}"
                                            class="form-control" placeholder="pinterest">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="email" value="{{ isset($entity->email)?$entity->email:'' }}"
                                            class="form-control" placeholder="E-mail">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-whatsapp"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="whatsapp" value="{{ isset($entity->whatsapp)?$entity->whatsapp:'' }}"
                                            class="form-control" placeholder="whatsapp">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="phone1" value="{{ isset($entity->phone1)?$entity->phone1:'' }}"
                                            class="form-control" placeholder="telefone 1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="phone2" value="{{ isset($entity->phone2)?$entity->phone2:'' }}"
                                            class="form-control" placeholder="telefone 2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Endereço</h2>
                    <p class="card-subtitle">
                        Informações de endereço da empresa que irão aparecer no site.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-map-marker-alt"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ isset($entity->address)?$entity->address:'' }}" name="address"
                                            class="form-control" placeholder="Endereço">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->number)?$entity->number:'' }}" name="number"
                                            class="form-control" placeholder="nº">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->city)?$entity->city:'' }}" name="city"
                                            class="form-control" placeholder="Cidade">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->district)?$entity->district:'' }}" name="district"
                                            class="form-control" placeholder="Bairro">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->state)?$entity->state:'' }}" name="state"
                                            class="form-control" placeholder="Estado">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->zipcode)?$entity->zipcode:'' }}" name="zipcode" class="form-control"
                                            placeholder="CEP">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                        </div>

                        <h2 class="card-title">Nossos valores</h2>
                        <p class="card-subtitle">
                            Texto que aparecerá na página sobre
                        </p>
                    </header>
                    <div class="card-body" style="display: block;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fas fa-align-left"></i>
                                                </span>
                                            </span>
                                            <input value="{{ isset($entity->valor_1)!=""?$entity->valor_1:"" }}" type="text" name="valor_1"
                                                class="form-control" placeholder="Valor 1">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fas fa-align-left"></i>
                                                </span>
                                            </span>
                                            <textarea class="form-control" name="valor_texto_1" id="" rows="2">{{ isset($entity->valor_texto_1)!=""?$entity->valor_texto_1:"" }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fab fa-instagram"></i>
                                                </span>
                                            </span>
                                            <input value="{{ isset($entity->valor_2)!=""?$entity->valor_2:"" }}" type="text" name="valor_2"
                                                class="form-control" placeholder="Valor 2">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fas fa-align-left"></i>
                                                </span>
                                            </span>
                                            <textarea class="form-control" name="valor_texto_2" id="" rows="2">{{ isset($entity->valor_texto_2)!=""?$entity->valor_texto_2:"" }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fab fa-linkedin"></i>
                                                </span>
                                            </span>
                                            <input type="text" value="{{ isset($entity->valor_3)!=""?$entity->valor_3:"" }}" name="valor_3"
                                                class="form-control" placeholder="Valor 3">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fas fa-align-left"></i>
                                                </span>
                                            </span>
                                            <textarea class="form-control" name="valor_texto_3" id="" rows="2">{{ isset($entity->valor_texto_3)!=""?$entity->valor_texto_3:"" }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fab fa-linkedin"></i>
                                                </span>
                                            </span>
                                            <input type="text" value="{{ isset($entity->valor_4)!=""?$entity->valor_4:"" }}" name="valor_4"
                                                class="form-control" placeholder="Valor 3">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fas fa-align-left"></i>
                                                </span>
                                            </span>
                                            <textarea class="form-control" name="valor_texto_4" id="" rows="2">{{ isset($entity->valor_texto_4)!=""?$entity->valor_texto_4:"" }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fab fa-linkedin"></i>
                                                </span>
                                            </span>
                                            <input type="text" value="{{ isset($entity->valor_5)!=""?$entity->valor_5:"" }}" name="valor_5"
                                                class="form-control" placeholder="Valor 3">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fas fa-align-left"></i>
                                                </span>
                                            </span>
                                            <textarea class="form-control" name="valor_texto_5" id="" rows="2">{{ isset($entity->valor_texto_5)!=""?$entity->valor_texto_5:"" }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                        </div>

                        <h2 class="card-title">Sobre a Sila Decor</h2>
                        <p class="card-subtitle">
                            Breve texto sobre a empresa
                        </p>
                    </header>
                    <div class="card-body" style="display: block;">
                        <div class="row">
                            <div class="col-lg-12">
                                <textarea class="form-control" name="texto_1" id="" rows="3">{{ isset($entity->texto_1)!=""?$entity->texto_1:"" }}</textarea>
                            </div>
                        </div>
                </section>

                <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            </div>

                            <h2 class="card-title">Sobre a Sila Decor</h2>
                            <p class="card-subtitle">
                                Breve texto sobre a empresa
                            </p>
                        </header>
                        <div class="card-body" style="display: block;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <textarea class="form-control" name="texto_2" id="" rows="3">{{ isset($entity->texto_2)!=""?$entity->texto_2:"" }}</textarea>
                                </div>
                            </div>
                    </section>

            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success">Salvar</button>
                </div>
            </section>

        </form>
    </div>
</div>
@endsection
