<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PainelController extends Controller
{

    public function index(Request $request) {

        $rota = $request->route('rota');

        $content = $this->url_get_contents('http://newesouth.logycware.com.br/'.$rota.'/');
        return view('pages.pontos', ['content' => $content]);

    }


    function url_get_contents($Url) {
        if (!function_exists('curl_init')) {
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if (!preg_match('/src="https?:\/\/"/', $result)) {
            $result = preg_replace('/src="(http:\/\/([^\/]+)\/)?([^"]+)"/', "src=\"http://newesouth.logycware.com.br\\3\"", $result);
        }
        if (!preg_match('/href="https?:\/\/"/', $result)) {
            $result = preg_replace('/href="(http:\/\/([^\/]+)\/)?([^"]+)"/', "href=\"http://" . $_SERVER['HTTP_HOST'] . "/pontos/\\3\"", $result);
            $result = str_replace("?documento=", "/", $result);
            $result = str_replace("arquivo?id=", "arquivo/", $result);
            $result = str_replace("arquivo?Id=", "arquivo/", $result);
            $result = str_replace("#resultado-pesquisa", "", $result);
        }

        $result = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $result);
        $result = preg_replace('#<link(.*?)>(.*?)#is', '', $result);
        return $result;
    }







}
