<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Informations extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'address',
        'number',
        'complement',
        'district',
        'zipcode',
        'city',
        'state',
        'whatsapp',
        'instagram',
        'facebook',
        'linkedin',
        'twitter',
        'pinterest',
        'email',
        'phone1',
        'phone2',
        'texto_1',
        'texto_2',
        'valor_1',
        'valor_2',
        'valor_3',
        'valor_4',
        'valor_5',
        'valor_texto_1',
        'valor_texto_2',
        'valor_texto_3',
        'valor_texto_4',
        'valor_texto_5'
    ];
}
